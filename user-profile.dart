
import 'package:flutter/material.dart';

// Source code used 
//https://github.com/ilagazo/Flutter_UserProfile
//https://stackoverflow.com/questions/56195771/create-user-profile-page-using-bloc-rxdart-and-flutter


class UserProfile {
  
   final int userID;
   final String firstName;
   final String lastName;
   final String email;
   final String phoneNumber;

   UserProfile({
     required this.userID,
     required this.firstName,
     required this.lastName,
     required this.email,
     required this.phoneNumber,

   });

factory UserProfile.fromMap(
  Map<String,dynamic> json) {
    return UserProfile(
      userID: json['user_id'] ,
      firstName: json['first_name'] ,
      lastName: json['last_name'] ,
      email: json['email'],
      phoneNumber:  json['phone_number'],
    );
  }
  }

class ProfileEdit extends StatefulWidget {
  const ProfileEdit ({Key? key}) : super(key: key);

  @override
  ProfileEditState createState() {
    return ProfileEditState();
  }
}
class ProfileEditState extends State<ProfileEdit> {
final _firstNameController = TextEditingController();
final _lastNameController = TextEditingController();
final _emailController = TextEditingController();
final _phoneController = TextEditingController();

 @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    super.dispose();
  }
 
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('USER-PROFILE',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
      body: Container(
        padding: const EdgeInsets.only(left: 16,top: 25,right: 16),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              const Text(
                'Edit Profile', textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25, fontWeight: FontWeight.w500,),
                ),
                const SizedBox(
                  height: 15,),
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 6,
                        color: Colors.transparent,
                      ),
                      shape: BoxShape.circle,
                      image: const DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          'assets/images/food.jpg',
                        )
                      )
                      )
                    ),const SizedBox(
                      height: 35,
                      
                    ),
                    Padding(
                      padding:  EdgeInsets.all(10),
                   child: 
                           TextField(
                             decoration: const InputDecoration(labelText: 'First Name', 
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.transparent
                                  )
                                )),
                                controller: _firstNameController, keyboardType: TextInputType.name,
                            )),
                            const SizedBox(
                              height: 35,),
                            TextField(
                             decoration:
                                const InputDecoration(labelText: 'Last Name',
                                 border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.transparent
                                  )
                                  )),
                            controller: _lastNameController, keyboardType: TextInputType.name,
                        ),
                        const SizedBox(
                              height: 35,),
                        TextField(
                             decoration:
                                const InputDecoration(labelText: 'Email',
                                 border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                  )
                                  )),
                                  controller: _emailController, keyboardType: TextInputType.emailAddress,        
                        ),
                       const SizedBox(
                              height: 35,),
                    TextField(
                      decoration:
                                const InputDecoration(labelText: 'Phone Number',
                                 border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                  )
                                  )),
                          controller: _phoneController, keyboardType: TextInputType.number,
                        ),
                         ElevatedButton(
                      onPressed: () { Navigator.pushNamed((context), '/Dashbaord',);
                    },
                      child: const Text(
                        'Update',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                        ]
                        )
                        )
             )
                            );
       
    }                        
}