import 'package:flutter/material.dart';
import 'package:login/user-profile.dart';
import 'dart:math';


// Source code used 
// https://github.com/shubie/dashboard
// https://maffan.medium.com/how-to-create-a-side-menu-in-flutter-a2df7833fdfb
// https://bitbucket.org/potondev/flutterhomescreen_01/src/master/lib/griddashboard.dart

Map<String, WidgetBuilder> routes = {
        '/Dashboard': (context) => const Dashboard(),
        '/MealPlan': (context) => MealPlan(),
        '/Workouts': (context) =>  Workouts(),
        '/ProfileEdit': (context) => const ProfileEdit(),
      };

void main(){}
class Dashboard extends StatelessWidget {
  const Dashboard ({Key? key}) : super(key: key);

  
  @override 
   Widget build(BuildContext context) {
    return Scaffold( 
      drawer: Navi(),
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('DASHBOARD',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
          body:
          
           ListView(
            padding: EdgeInsets.all(10),
            
        children: <Widget>[
          ListTile(
            leading: Text(
              'DIET VARIETIES',
              style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.w900),
            ), title: Image.asset('assets/images/food.jpg'),
            onTap:() { Navigator.pushNamed((context), '/MealPlan',);}),
            ListTile(
            leading: Text(
              'WORKOUTS',
              style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.w900),
            ), title: Image.asset('assets/images/cardio.jpg'),
            onTap:() { Navigator.pushNamed((context), '/Workouts',);})
            ]
            
                      )
                      );
    }

   }     
  

class Navi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            child: Text(
              'Side menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 76, 163, 175),
                image: DecorationImage(fit: BoxFit.cover,
                image: AssetImage('assets/images/cook.jpg'))
                ),
                
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Welcome'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
             onTap: () { Navigator.pushNamed((context), '/ProfileEdit',);
                    },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('Feedback'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.output_outlined),
            title: Text('Logout'),
            onTap: () {Navigator.pushNamed((context), '/',);
            },
          ),
        ],
      ),
    );
  }
}

class Items {
  String title;
  String image;
  Items({required this.title, required this.image});
}

class MealPlan extends StatelessWidget {
   Items item1 = new Items(
    title: "LOW-CARBOHYDRATE",
    image: "assets/images/Low-carb.jpg",
  );

  Items item2 = new Items(
    title: "KETOGENIC",
    image: "assets/images/keto.jpg",
  );
  Items item3 = new Items(
    title: "VEGETARIAN",
    image: "assets/images/vegetarian.jpg",
  );
   Items item4 = new Items(
    title: "RAW-FOODISM",
    image: "assets/images/raw-foodism.jpg",
  );
  Items item5 = new Items(
    title: "GLUTEN-FREE",
    image: "assets/images/gluten-free.jpg",
  );
  Items item6 = new Items(
    title: "VEGANISM",
    image: "assets/images/vegan.jpg",
  );
   Items item7 = new Items(
    title: "LOW-FAT",
    image: "assets/images/low-fat.jpg",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4, item5, item6, item7];
    var color = Colors.transparent;
    return Scaffold(
        appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('DIET TYPES',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
        body: 
         GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.transparent, borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    data.image,
                    width: 250,fit: BoxFit.cover,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.title,
                    style: TextStyle(
                            color: Color.fromARGB(255, 10, 10, 10),
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                  ),
                 
                ],
              ),
            );
          }).toList()),
    );
  }
}


class Workouts extends StatelessWidget {
   Items item1 = new Items(
    title: "CARDIO",
    image: "assets/images/cardio.jpg",
  );

  Items item2 = new Items(
    title: "FUNCTIONAL STRENGTH TRAINING",
    image: "assets/images/functional-strength-training.jpg",
  );
  Items item3 = new Items(
    title: "HIIT",
    image: "assets/images/hiit.jpg",
  );
   Items item4 = new Items(
    title: "WEIGHT TRAINING",
    image: "assets/images/weight-training.jpg",
  );
  Items item5 = new Items(
    title: "PILATES",
    image: "assets/images/pilates.jpg",
  );
 

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4, item5,];
    var color = Colors.transparent;
    return Scaffold(
        appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('WORKOUTS',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
        body: 
         GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.transparent, borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    data.image,
                    width: 250,fit: BoxFit.cover,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.title,
                    style: TextStyle(
                            color: Color.fromARGB(255, 10, 10, 10),
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                  ),
                 
                ],
              ),
            );
          }).toList()),
    );
  }
}