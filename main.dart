
import 'package:flutter/material.dart';
import 'package:login/dashboard.dart';
import 'package:login/user-profile.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';


//https://github.com/RaghavTheGreat1/material_you
//Source code used 
//


void main() {
  runApp(const MyApp());
}

Map<String, WidgetBuilder> routes = {
        '/': (context) => const LoginScreen(),
        '/Create New Account': (context) => const CreateNewAccount(),
        '/Dashboard': (context) => const Dashboard(),
        '/MealPlan': (context) =>  MealPlan(),
        '/Workouts': (context) =>  Workouts(),
        '/ProfileEdit': (context) => const ProfileEdit(),
      
      };

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pristine Fitness',
      theme: ThemeData( primarySwatch: Colors.teal, ),
      

      initialRoute: '/',
      routes: {
        '/': (context) => const LoginScreen(),
        '/Create New Account': (context) => const CreateNewAccount(),
        '/Dashboard': (context) => const Dashboard(),
        '/MealPlan': (context) =>  MealPlan(),
        '/Workouts': (context) =>  Workouts(),
        '/ProfileEdit': (context) => const ProfileEdit(),
      }
      );
 }
}
      
 class LoginScreen extends StatefulWidget {
     const LoginScreen ({Key? key}) : super(key: key);

   @override 
   LoginScreenState createState() {
   return LoginScreenState();
 }
 }
 class LoginScreenState extends State<LoginScreen> {
   final _emailController = TextEditingController();

 @override  
 void dispose() {
 _emailController.dispose();
 }


 @override
  Widget build(BuildContext context){
   return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('LOGIN',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration:
             BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(colors: [
               Color.fromARGB(255, 62, 133, 133),
               Color.fromARGB(255, 0, 29, 29),
               ]),
            ),
                  child: Builder( builder: (context) {
                    return ListView(
                    children: <Widget> [
                      Padding(
                      padding:  const EdgeInsets.only(top: 60.0),
                   child: Center( 
                     child: Container( width: 200, height: 150,
                     decoration: BoxDecoration(
                       color: Colors.transparent, borderRadius: BorderRadius.circular(50.0)
                     ),
                     child: Image.asset('assets/images/8e628c17fdf34f678eecabc9312ddc0d (1).png'),))
                     ),
                       Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:15.0,bottom:0),
                   child: TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'User-Name/Email Address',
                   ),  
                   controller: _emailController, keyboardType: TextInputType.emailAddress,
                  )
                 ),
               Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:45.0,bottom:0),
                   child: const TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                   )  
                  )
                 ),
                 SizedBox(height: 100),
                 Container(height: 30, width: 10, decoration: BoxDecoration(
                   color: Color.fromARGB(255, 219, 231, 241), borderRadius: BorderRadius.circular(10)
                 ),
                 child: 
                  TextButton( onPressed: () { Navigator.pushNamed((context), '/Dashboard',);
                    },
                    child: Text('login', style: TextStyle(color: Color.fromARGB(255, 2, 2, 2)),
                    )
                    )
                        ),
                        SizedBox(
                          height: 130,),
                        TextButton( onPressed: () { Navigator.pushNamed((context), '/Create New Account',);
                    },
                    child: const Text('New User? Create New Account', style: TextStyle(color:Color.fromARGB(255, 22, 22, 22)),),
                        ),

                           ]
                        );
                 }
                 ),
        )
        )
        );
                   }
                 }

  class CreateNewAccount extends StatefulWidget {
     const CreateNewAccount ({Key? key}) : super(key: key);

   @override 
   CreateNewAccountState createState() {
   return CreateNewAccountState();
 }
 }
 class CreateNewAccountState extends State<CreateNewAccount> {
   final _emailController = TextEditingController();

 @override  
 void dispose() {
 _emailController.dispose();
 }
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 62, 133, 133),
        title: const Text ('REGISTRATION',
        style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white,
        )
        ),
        centerTitle: true,
        ),
          body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration:
             BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(colors: [
               Color.fromARGB(255, 62, 133, 133),
               Color.fromARGB(255, 0, 29, 29),
               ]),
            ),
              child: Builder( builder: (context) {
                    return ListView(
                    children: <Widget> [
                      Padding(
                      padding:  const EdgeInsets.only(top: 60.0),
                   child: Center( 
                     child: Container( width: 200, height: 150,
                     decoration: BoxDecoration(
                       color: Colors.transparent, borderRadius: BorderRadius.circular(40.0)
                     ),
                     child: Icon( Icons.person, size: 40,),))),
                     Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:15,bottom:0),
                   child:
                    const TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name and Surname',
                   )  
                    ),),
                     Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:25.0,bottom:0),
                   child: TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email Address',
                   ), 
                    controller: _emailController, keyboardType: TextInputType.emailAddress,
                  )
                 ),
                     Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:35,bottom:0),
                   child: const TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                   )  
                  )
                 ),
                    Padding(padding:  const EdgeInsets.only(
                         left: 15.0,right:15.0,top:30,bottom:0),
                   child: const TextField(
                   decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Confim Password',
                   )  
                  )
                 ),
                    const SizedBox(
                      height: 25,
                    ),
                    TextButton(
                  child: const Text(
                    'Sign Up',
                    style: TextStyle
                                (color: Color.fromARGB(255, 41, 74, 255), fontWeight: FontWeight.bold, fontSize: 15,), 
                                ),
                     onPressed: () { Navigator.pushNamed((context), '/Dashboard',);
                    },
                        ),
                    const SizedBox(
                      height: 130,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Already have an account?',
                          style: TextStyle( ),
                        ),
                        TextButton( onPressed: () { Navigator.pushNamed((context), '/', arguments: '1');
                        },
                    child: const Text('Login'),
                        ),
                       ]
                        ),
          ]
          );
  }
  )
  )
  ),
                );
  }
}
